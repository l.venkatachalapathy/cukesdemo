package cucumberTest;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = "Feature", glue = { "stepDefinition" }
// ,dryRun = true
// ,monochrome=false
// ,format = {"html:Reports"} // doubt
// ,format = {"json:Reports/cucumber.xml"}
)
public class TestRunner {

}
