package cucumberTest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class seleniumTest {
	private static WebDriver driver = null;

	public static void main(String[] args) {
		// Create a new instance of the Firefox driver
		// System.setProperty("webdriver.gecko.driver",
		// "D:/Lavanya/geckodriver.exe");
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		// Put a Implicit wait, this means that any search for elements on the
		// page could take the time the implicit wait is set for before throwing
		// exception

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		// Launch the Mercury demo website
		driver.get("http://newtours.demoaut.com/");

		// Find the element that's ID attribute is 'log' (Username)

		// Enter Username on the element found by above desc.

		driver.findElement(By.name("userName")).sendKeys("mercury");

		// Find the element that's ID attribute is 'pwd' (Password)

		// Enter Password on the element found by the above desc.

		driver.findElement(By.name("password")).sendKeys("mercury");

		// Now submit the form. WebDriver will find the form for us from the
		// element

		driver.findElement(By.name("login")).click();

		// Print a Log In message to the screen

		System.out.println("Login Successfully");

		// Find the element that's ID attribute is 'account_logout' (Log Out)

		driver.findElement(By.linkText("SIGN-OFF")).click();

		// Print a Log In message to the screen

		System.out.println("LogOut Successfully");

		// Close the driver

		driver.quit();
	}

}
